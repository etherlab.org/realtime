## Git Hooks

```bash
pre-commit install --hook-type post-commit --hook-type post-checkout --hook-type post-merge
pre-commit run --hook-stage manual
```
